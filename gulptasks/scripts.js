// Работа со скриптами
const
	gulp    = require('gulp'),
	babel   = require('gulp-babel'),
	concat  = require('gulp-concat'),
	plumber = require('gulp-plumber'),
	srcmaps = require('gulp-sourcemaps');


// Обрабатываем пользовательские скрипты
gulp.task('scripts:user', () => {

	return gulp
		.src(['./source/base/*.js', './source/blocks/**/*.js']) // Берем пользовательские скрипты и скрипты блоков (см. BEM_)
		.pipe( plumber() )										// Запускаем отслеживание ошибок
		.pipe( babel() )										// Прогоняем скрипты через Babel для перевод ES6 в валидный ES5
		.pipe( srcmaps.init() )									// Инициализируем сорсы
		.pipe( concat('scripts.js') )							// Склеиваем скрипты в единый файл	
		.pipe( srcmaps.write('./.maps') )						// Сохраняем сорсы в папку сборки
		.pipe( gulp.dest('./public/js') );						// Сохраняем результат в папку билда со скриптами

});


// Main task
gulp.task('scripts', ['scripts:user']); // Регистрируем таск с обработкой всех скриптов