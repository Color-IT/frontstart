// Обработка стилей

const
    gulp     = require('gulp'),
    stylus   = require('gulp-stylus'),
    concat   = require('gulp-concat'),
    plumber  = require('gulp-plumber'),
    srcmaps  = require('gulp-sourcemaps');
    
const
    postcss  = require('gulp-postcss'),
    csso     = require('postcss-csso'),
    prefixer = require('autoprefixer');


// Массив плагинов PostCSS
const postProcessors = [
    prefixer,  // Добавляет вендорные префиксы к не утвердившимся в стандарте свойствам
    csso       // Минификация файла стилей
];


// Регистрация таска для стилей
gulp.task('styles', () => {

    return gulp
        .src(['./source/base/main.styl', './source/app/modules/**/*.styl']) // Берем файлы стилей .styl
        .pipe( plumber() )						// Запускаем отслеживание ошибок
        .pipe( stylus() )						// Транспайлим STYL в CSS
        .pipe( srcmaps.init() )					// Инициализируем сорсы
        .pipe( concat('styles.css') )			// Склеиваем все стили в один файл
        .pipe( postcss(postProcessors) )		// Прогоняем файл стилей через плагины PostCSS
        .pipe( srcmaps.write('./.maps') )		// Сохраняем сорсы в файл
        .pipe( gulp.dest('./public/css/') );    // Сохраняем результат работы с папку со стилями

});
